#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import boto3
import json
import logging
from pprint import pformat

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig(
    level=logging.INFO,
    format='%(levelname)-8s %(message)s',
)
# Silence boto3 logging:
logging.getLogger('boto3').setLevel(logging.WARNING)
logging.getLogger('botocore').setLevel(logging.WARNING)

session = boto3.session.Session(region_name='us-east-1')  # Fixed endpoint since there are only 2 of these, us-east-1 and another high-latency one...
pricing = session.client('pricing')

# Because AWS has retardedly decided to use a different name for regions in this API... seriously AWS, wtf, at least also include the other format....
regionmap = {
    "ap-northeast-1": "Asia Pacific (Tokyo)",
    "ap-northeast-2": "Asia Pacific (Seoul)",
    "ap-northeast-3": "Asia Pacific (Osaka-Local)",
    "ap-south-1"    : "Asia Pacific (Mumbai)",
    "ap-southeast-1": "Asia Pacific (Singapore)",
    "ap-southeast-2": "Asia Pacific (Sydney)",
    "ca-central-1"  : "Canada (Central)",
    # "cn-north-1"    : "China",
    # "cn-northwest-1": "China",
    "eu-central-1"  : "EU (Frankfurt)",
    "eu-west-1"     : "EU (Ireland)",
    "eu-west-2"     : "EU (London)",
    "eu-west-3"     : "EU (Paris)",
    "sa-east-1"     : u'South America (São Paulo)',
    "us-east-1"     : "US East (N. Virginia)",
    "us-east-2"     : "US East (Ohio)",
    "us-west-1"     : "US West (N. California)",
    "us-west-2"     : "US West (Oregon)",
}


def getOndemandPrice(instances, region):
    if region not in regionmap:
        logging.warning("Unknown region [{}], edit this script!".format(region))
        return -1
    location = regionmap[region]
    results = {}
    for instance_type in instances:
        response = pricing.get_products(
            ServiceCode='AmazonEC2',
            Filters=[
                {'Type': 'TERM_MATCH', 'Field': 'instanceType'  , 'Value': instance_type},
                {'Type': 'TERM_MATCH', 'Field': 'location'      , 'Value': location},
                {'Type': 'TERM_MATCH', 'Field': 'tenancy'       , 'Value': 'shared'},
                {'Type': 'TERM_MATCH', 'Field': 'operation'     , 'Value': 'RunInstances'},
                {'Type': 'TERM_MATCH', 'Field': 'capacitystatus', 'Value': 'UnusedCapacityReservation'},
            ],
            MaxResults=1
        )

        # We get a list of products with attributes and along with those are one or more 'terms' with pricing. For example Reserved Instance options and Ondemand.
        # For now assume we only care about the first (should be only) entry in the list, pick the ondemand one.
        for price in response['PriceList']:
            price = json.loads(price)
            ondemandprice = ''
            # Debug:
            # logging.debug(pformat(price['product']['attributes']))
            for p in price['terms']['OnDemand']:
                for dim in price['terms']['OnDemand'][p]['priceDimensions']:
                    ondemandprice = price['terms']['OnDemand'][p]['priceDimensions'][dim]['pricePerUnit']['USD']
            logging.info("{}".format(ondemandprice))
            results[instance_type] = ondemandprice
    return results


def lambda_handler(event, context):
    res = None
    if 'region' in event and 'instancetype' in event:
        res = getOndemandPrice([event['instancetype']], event['region'])
        if (len(res.keys()) == 1):
            return list(res.values())[0]
    else:
        raise UserWarning('Invalid input, expected "region" (may be filled in with AZ) and "instancetype" in payload.')
    return res


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='AWS OnDemand Price Checker')
    parser.add_argument('--region', metavar='region', help='region to check, i.e. eu-west-1 or us-east-1', default='eu-west-1')
    parser.add_argument('instance_type', metavar='instance_type', nargs='+', help='instance type(s) to check, i.e. m5.large t3.nano')
    args = parser.parse_args()
    getOndemandPrice(args.instance_type, args.region)
